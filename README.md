# LP Eureciclo 

Para melhor manutenção do código temos duas versões de JS e CSS.

As pastas js-dev e css-dev contém os arquivos sem minificação, compressão e adição de autoprefixer(para css). Nas pastas css e js está o arquivo minificado e unificado que é chamado no index.html.

## Para executar a compressão:

Para facilitar a compressão e unificação usamos uma automatização do gulp. Para executá-la é necessário ter o npm instalado. Segue os comandos:

### Para instalar:
    npm install

### Para executar:
    npm start

Uma vez executado o npm irá ficar rodando e sempre que salvar um arquivo na pasta js-dev ou css-dev, os arquivos nas pastas css e js serão atualizados. 

Recomenda-se rodar o npm antes de começar a trabalhar. 
