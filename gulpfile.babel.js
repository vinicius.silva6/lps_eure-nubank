import gulp from 'gulp';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import uglify from 'gulp-uglify-es';
import browserSync from 'browser-sync';
import fs from 'fs-extra';
var path = require('path');
var rename = require('gulp-rename');
var merge = require('merge-stream');
var concat = require('gulp-concat');



var scriptsPath = 'js-dev/';
var scriptsPathDest = 'js/';
var cssPath = 'css-dev';
var cssPathDest = 'css/';


export const styles = () => {
    return gulp.src(path.join(cssPath, '/*.css'))
    .pipe(autoprefixer())
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest(cssPathDest));

}

export const scripts = () => {
        return gulp.src(path.join(scriptsPath, '/*.js'))
          .pipe(uglify())
          .pipe(concat('main.min.js'))
          .pipe(gulp.dest(scriptsPathDest));
}

export const serve = (done) => {
	server.init({
		proxy: "http://localhost/Uninter.Com/"
	});
	done();
}


export const watch = () => {
    gulp.watch('css-dev/**/*.css', gulp.series(styles));
    gulp.watch('js-dev/**/*.js', gulp.series(scripts));
}

export default gulp.series(watch);
 