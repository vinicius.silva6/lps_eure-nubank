

var glider_home_depositions = document.querySelector('.glider-wrapper');
if(glider_home_depositions){
  new Glider(glider_home_depositions, {
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: '.glider-pagination',
    draggable: true,
    easing: function (x, t, b, c, d) {
      return c*(t/=d)*t + b;
    },
    responsive: [
      {
        breakpoint: 905,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
        }
      }
    ]
  });
}
