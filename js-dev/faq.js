let questions = document.querySelectorAll(".questions");

questions.forEach((element) => {
  element.addEventListener("click", function () {
    let id = element.getAttribute("data-id");
    let answer = document.querySelector(".answer-" + id);
    answer.classList.toggle("none");
  });
});
